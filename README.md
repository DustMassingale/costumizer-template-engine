# README #



### What is this repository for? ###

This is a repository for a simple content management system using AngularJS. An admin has the ability to add pages, using web editors. The admin also has access to basic usage reports.

### How do I get set up? ###
This is a PHP project, see the dependencies on how to deploy. For development,
This was designed with Visual Studio 2013 running 
Devsense PHP-Tools http://www.devsense.com/products/php-tools

Additional support in developing this was provided by Jetbrains who donated Re-Sharper and PHPstorm.

Database software provided by http://www.navicat.com
![Premium_72dpi_2230x763.jpg](https://bitbucket.org/repo/aG4a4G/images/656525731-Premium_72dpi_2230x763.jpg)

#dependencies. 
The project runs on a web-server supporting php 5.x with sqlite 3 support, using PDO.

### Contribution guidelines ###

Currently this is a small project. Please discuss with Dustin before performing any  checkins.

### Open License ###
Copyright 2017 Dustin Massingale

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. 

### FAQ ###
Why am I getting the exception "could not find driver?" Answer: This usually means that the sqlite driver is not installed. [Click Here for an Example](http://stackoverflow.com/questions/8803728/pdo-sqlite-could-not-find-driver-php-file-not-processing)

### Who do I talk to? ###

Dustin Massingale - you can also comment on the http://www.dustinmassingale.com/itLib.php site.