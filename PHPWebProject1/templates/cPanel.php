<?php
/* Name: cPanel.php V1
 * Description: A series of controls to adjust settings of a web page. It is secured on the server side, to prevent unauthorized access.
 * 
 * History: 
 * VERSION  DATE         AUTHOR        DESCRIPTION
 * 1        2014-11-17   DMASSINGALE CREATED
 * 
 * */
session_start();
if ($_SESSION["ID"]){
?>

<!--<p>The action specified is <?=$_GET["act"]?></p>-->

<div id="output" class="alert alert-info" style="display: none"></div>
<section class="col-sm-offset-1" id="content" style="display: none" data-ng-controller="contentMgrController">
    
    <link href="/Content/jquery.cleditor.css" rel="stylesheet" />
    <h1>Content Manager</h1>
    <table class="table">
        <thead>
            <tr>
                <th>id</th>
                <th>title</th>
                <th></th>
                <th>shown</th>
                <th>order</th>
            </tr>
        </thead>
        <tbody>

            <tr data-ng-repeat="j in cust">
                <td>{{j.id}}</td>
                <td id="titleId{{j.id}}">{{j.title}}</td>
                <td>
                    <!-- <button class="btn btn-default" data-type="editorBtn" onclick="openTemplate(1)">JQT Editor</button>-->
                    <button id="{{j.id}}" class="btn btn-default" data-type="editorBtn" onclick="openTemplate(this.id,'cl')"><span class="ui-icon ui-icon-pencil" style="float: left"></span>Editor View</button>
                </td>
                <td>{{j.show}}</td>
                <td>{{j.order}}</td>
            </tr>
        </tbody>
    </table>
    <div id="editorWrapperParent">
        <form id="editorWrapper" class="form">
        </form>
    </div>
    
    <script src="/js/jquery.cleditor.min.js"></script>
    <script src="/js/jquery.cleditor.icon.min.js"></script>
    <script src="/js/jquery.cleditor.table.min.js"></script>

    <script>


        function saveTemplate(templateId) {
            $('#output').show().text("processing template, please wait.");

            var jqxhr = $.post("/api/v1/base.php?method=contentPost&id=" + templateId,
                $("form#editorWrapper").serialize(),
                function () {
                    //redirect to the corrected template page
                    window.location.href = "/#/templates/" + templateId;
                   
                });
        }
        function cancelTemplate() {

            $('button[data-type="editorBtn"]').show();
            $('#editorWrapperParent').hide();
        }

        function openTemplate(LocalTemplateId, editor) {
            var templateTitle = $('#titleId' + LocalTemplateId).text();
            //add form information
            $('form#editorWrapper')
              .html('<div class="form-group"><label for="title" >Page Title Shown in Links</label><input class="md-colspan-3" type="text" class="form-control" name="title" required="required" value="' +
              templateTitle + '"/></div>' +
              '<div class="form-group"><label for="Content" ">Content Displayed for Users:</label><br/>' +
              '<textarea id="editable" name="content" class="form-control" ></textarea></div>');
            //show the form
            $('#editorWrapperParent').show();
            //set the global template variable
            globe.templateId = LocalTemplateId;
            $('#showTemplateId').text(LocalTemplateId);
            //clean any existing formats from the editor

            $.get("/templates/generator.php?id=" + LocalTemplateId, function (data) {

                if (editor == "cl") {
                    $('textarea#editable').val(data).cleditor();
                    $('.cleditorToolbar').append('<div id="custControls" class="cleditorGroup"></div>');

                    $('#custControls:first')
                        .html('<button class="btn btn-primary" onclick="saveTemplate(' + LocalTemplateId + ')">Apply</button>' +
                        '<button class="btn btn-default" onclick="cancelTemplate()">Cancel</button><label for="show">Show</label><input type="checkbox" name="show" checked/>'
                    );
                } else {
                    $('textarea#editable').val(data).jqte();
                    //add the save button
                    $('div.jqte_toolbar.unselectable').append('<span id="custControls"></span>');

                    $('span#custControls:first')
                        .html('<button class="btn btn-default" onclick="saveTemplate()">Save</button>' +
                        '<button class="btn btn-default" onclick="cancelTemplate()">Cancel</button>'
                    );
                }
                //get rid of the editor buttons
                $('button[data-type="editorBtn"]').hide();

            });
        }

        function contentMgrController($scope, $http) {

            $http.get("/api/v1/base.php?method=content",
              { header: { 'Content-Type': 'application/json' } }
              )
              .success(function (response) { $scope.cust = response; });

        }

    </script>
</section>
<section id="users" style="display: none" data-ng-controller="userController">
    <h1>User Manager</h1>
    <div class="panel panel-default">
        <div class="panel-heading">Current Users</div>
        <table class="table">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th style="display: none" data-ng-hide="">edit</th>
                </tr>
            </thead>

            <tr data-ng-repeat="i in names">
                <td>{{i.id}}</td>
                <td>{{i.firstname}}</td>
                <td>{{i.lastname}}</td>
                <td>{{i.email}}</td>
                <td style="display: none" id="individual" onclick="editRow(this)" data-editid="{{i.id}}">EDIT</td>
            </tr>
        </table>
    </div>
</section>
<section id="analyze" style="display: none">
    <h1>Analytics</h1>
</section>

<script type="text/javascript">
    $(function () {

        $('section#<?=$_GET["act"]?>').show();
    });
    function editRow(rowNum) {
        $('div#output').show().text('Editing Row: ' + $(rowNum).attr("data-editId") + ' is not available in this edition of costumizer.');
    }
    function userController($scope, $http) {
        $http.get("/api/v1/base.php?method=user",
    { header: { 'Content-Type': 'application/json' } }
    )
    .success(function (response) { $scope.names = response; });


    }



</script>










<?php } else {
    
    echo "Your session is invalid or has expired. Please log in.";   
    
}?>