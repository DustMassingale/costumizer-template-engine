<?php
$quickvar = "myval";
?>
<html>
<head>
<script type="text/javascript">
//Global Variables
function globe(){
this.quickvar = "<?php $quickvar ?>";
};
</script>
</head>
<body>
    <h1>quickTemplate</h1>
    <div data-ng-controller="baseController">
        Welcome {{username}}
        <br />
        First Name: {{firstname}}
        <br />
        Last Name: {{lastname}}
        <br/>
        Server side code provides the value: {{globe.quickvar}}
        
    </div>
</body>
</html>
