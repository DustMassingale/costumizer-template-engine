﻿/// <reference path="~/js/jquery-rc4crypt.js" />
/// <reference path="~/Scripts/angular.js" />


var module = angular.module("costumizer", ['ngRoute']);


module.config(function($routeProvider) {
    $routeProvider.when("/home", {
        controller: "", //I was calling this index controller, but that is already defined on the home page.
        //templateUrl: "templates/home.html"
        templateUrl: "templates/generator.php?id=1"

        
    });
    //-- User configurable templates --//
    //$routeProvider.when("/templates/1", {
    //    controller: "contentController",
    //    templateUrl: "templates/generator.php?id=1"
    //});
    for (var i1 = 2; i1 <= ($('.num-templates').length + 2); i1++) {
        var myId = i1;
        $routeProvider.when("/templates/" + myId, {
            controller: "contentController",
            templateUrl: "templates/generator.php?id=" + myId
        });
    }//for
    $routeProvider.when("/login", {
        controller: "", //I was calling this index controller, but that is already defined on the home page.
        templateUrl: "templates/login.html"
    });
    //-- Admin Templates --//
    var cPanel = ["content", "users", "analyze"];
    
    for (var i2 = 0; i2 <= (cPanel.length + 1); i2++) {
        $routeProvider.when("/panel/" + cPanel[i2], {
            controller: "",
            templateUrl: "templates/cPanel.php?act=" + cPanel[i2]
        });
    }//for


    $routeProvider.otherwise({ redirectTo: "/home" });
});

function contentController($scope, $routeParams) {
   // alert($routeParams.id);
}

function personController($scope) {
    $scope.firstName = "John";
    $scope.lastName = "Doe";
}
function indexController($scope, $http) {
    
    $scope.username = "red";
    $scope.firstname = "yellow";
    $scope.lastname = "redLast";
    $scope.appName = globe.appName;
    
    $http.get("/api/v1/base.php?method=user",
        { header: { 'Content-Type': 'application/json' } }
        )
        .success(function (response) { $scope.names = response; });

    $http.get("/api/v1/base.php?method=content",
      { header: { 'Content-Type': 'application/json' } }
      )
      .success(function(response) {
            $scope.cust = response;
        });
    //var encrypted = //CryptoJS.TripleDES.encrypt("Message", "Secret Passphrase");
    var encrypted = $.rc4EncryptStr("apple", "Secret Passphrase");

    //var decrypted = CryptoJS.TripleDES.decrypt(encrypted, "Secret Passphrase");
    var decrypted = $.rc4DecryptStr(encrypted, "Secret Passphrase");
    $scope.quickvar2 = globe.quickvar;
    $scope.passwordSecret = encrypted;
    $scope.password = decrypted;
}

//When the page loads, and a user is loged in, show the admin controls
if (globe.userId != "") {
    $('#adminControlls').show();
    $('#loginLink').hide();
    $('#logoutLink').show();
} else {
    $('#loginLink').show();
    $('#logoutLink').hide();
}