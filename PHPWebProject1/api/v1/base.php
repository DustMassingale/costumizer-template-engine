<?php
//-- dustin added session start --//

session_start();



function cleanDBtext($dbText){
    
    $dbText = str_replace("\\\"","\"",$dbText);
    $dbText = str_replace("<script","",$dbText);//a basic replacement to prevent jscript injection, might be better to regex
    $dbText = str_replace("<SCRIPT","",$dbText);//a basic replacement, might be better to regex


    return $dbText ;
}
/*
API Demo

This script provides a RESTful API interface for a web application

Input:

$_GET['format'] = [ json | html | xml ]
$_GET['method'] = []

Output: A formatted HTTP response

Author: Mark Roland

History:
11/13/2012 - Created

 */

// --- Step 1: Initialize variables and functions

/**
 * Deliver HTTP Response
 * @param string $format The desired HTTP response content type: [json, html, xml]
 * @param string $api_response The desired HTTP response data
 * @return void
 **/
function deliver_response($format, $api_response){
    
    // Define HTTP responses
    $http_response_code = array(
        200 => 'OK',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        403 => 'Forbidden',
        404 => 'Not Found'
    );
    
    // Set HTTP Response
    header('HTTP/1.1 '.$api_response['status'].' '.$http_response_code[ $api_response['status'] ]);
    
    // Process different content types
    if( strcasecmp($format,'json') == 0 ){
        
        // Set HTTP Response Content Type
        header('Content-Type: application/json; charset=utf-8');
        
        // Format data into a JSON response
        $json_response = json_encode($api_response);
        
        // Deliver formatted data
        echo $json_response;
        
    }elseif( strcasecmp($format,'xml') == 0 ){
        
        // Set HTTP Response Content Type
        header('Content-Type: application/xml; charset=utf-8');
        
        // Format data into an XML response (This is only good at handling string data, not arrays)
        $xml_response = '<?xml version="1.0" encoding="UTF-8"?>'."\n".
            '<response>'."\n".
            "\t".'<code>'.$api_response['code'].'</code>'."\n".
            "\t".'<data>'.$api_response['data'].'</data>'."\n".
            '</response>';
        
        // Deliver formatted data
        echo $xml_response;
        
    }else{
        
        // Set HTTP Response Content Type (This is only good at handling string data, not arrays)
        header('Content-Type: text/html; charset=utf-8');
        
        // Deliver formatted data
        echo $api_response['data'];
        
    }
    
    // End script process
    exit;
    
}

// Define whether an HTTPS connection is required
$HTTPS_required = FALSE;

// Define whether user authentication is required
$authentication_required = TRUE; //forcing valid sessions

// Define API response codes and their related HTTP response
$api_response_code = array(
    0 => array('HTTP Response' => 400, 'Message' => 'Unknown Error'),
    1 => array('HTTP Response' => 200, 'Message' => 'Success'),
    2 => array('HTTP Response' => 403, 'Message' => 'HTTPS Required'),
    3 => array('HTTP Response' => 401, 'Message' => 'Authentication Required'),
    4 => array('HTTP Response' => 401, 'Message' => 'Authentication Failed'),
    5 => array('HTTP Response' => 404, 'Message' => 'Invalid Request'),
    6 => array('HTTP Response' => 400, 'Message' => 'Invalid Response Format')
);

// Set default HTTP response of 'ok'
$response['code'] = 0;
$response['status'] = 404;
$response['data'] = NULL;

// --- Step 2: Authorization

// Optionally require connections to be made via HTTPS
if( $HTTPS_required && $_SERVER['HTTPS'] != 'on' ){
    $response['code'] = 2;
    $response['status'] = $api_response_code[ $response['code'] ]['HTTP Response'];
    $response['data'] = $api_response_code[ $response['code'] ]['Message'];
    
    // Return Response to browser. This will exit the script.
    deliver_response($_GET['format'], $response);
}

// Optionally require user authentication
if( $authentication_required ){
    
    //if( empty($_POST['username']) || empty($_POST['password']) ){
    if( empty($_SESSION['ID'])){
        $response['code'] = 3;
        $response['status'] = $api_response_code[ $response['code'] ]['HTTP Response'];
        $response['data'] = $api_response_code[ $response['code'] ]['Message'];
        
        // Return Response to browser
        deliver_response($_GET['format'], $response);
        
    }
    
    // Return an error response if user fails authentication. This is a very simplistic example
    // that should be modified for security in a production environment
    //elseif( $_POST['username'] != 'foo' && $_POST['password'] != 'bar' ){
    //    $response['code'] = 4;
    //    $response['status'] = $api_response_code[ $response['code'] ]['HTTP Response'];
    //    $response['data'] = $api_response_code[ $response['code'] ]['Message'];
        
    //    // Return Response to browser
    //    deliver_response($_GET['format'], $response);
        
    //}
    
}

// --- Step 3: Process Request

// Method A: Say Hello to the API
if( strcasecmp($_GET['method'],'hello') == 0){
    $response['code'] = 1;
    $response['status'] = $api_response_code[ $response['code'] ]['HTTP Response'];
    $response['data'] = 'Hello World';
    deliver_response($_GET['format'], $response);
}

// Method B: Deliver a basic set of user info
else if( strcasecmp($_GET['method'],'user') == 0){
    $staticVar = "a static variable";
    session_start();
    $sessionVar = "SESSION ".$_SESSION["ID"]."";
    
    try
    {
        //open the database
        $db = new PDO('sqlite:contentDb_PDO.sqlite');
        
        $result = $db->query('SELECT id,email,name,permission,lastname,firstname FROM users');
        $myData = '[';
        foreach($result as $row)
        {
            $myData .= '{
       "id": "'.$row['id'].'",
       "email": "'.$row['email'].'",
       "username": "'.$row['name'].'",
       "permission": "'.$row['permission'].'",
       "lastname": "'.$row['lastname'].'",
       "firstname": "'.$row['firstname'].'"},';
        }      
        $myData = rtrim($myData,",");
        $myData .=']';
        $response['code'] = 1;
        $response['status'] = $api_response_code[ $response['code'] ]['HTTP Response'];
        $response['data'] = $myData;
        deliver_response($_GET['format'], $response);

        // close the database connection
        $db = NULL;
    }
    catch(PDOException $e)
    {
        print 'Exception : '.$e->getMessage();
    }   
    
 
     //Content is being written
} 
//Method user login
else if( strcasecmp($_GET['method'],'login') == 0){
    
    $perms = "";//used to hold permission level
    try
    {
        //open the database
        $db = new PDO('sqlite:contentDb_PDO.sqlite');
        
        $result = $db->query('SELECT id,permission FROM users WHERE name="'.$_POST['name'].'" AND password="'.$_POST['pass'].'"');
      
        foreach($result as $row)
        {
            $_SESSION['USERID'] = $row['id'];            
            $perms = $row['permission'];            
        }
        
        // close the database connection
        $db = NULL;
    }
    catch(PDOException $e)
    {
        print 'Exception : '.$e->getMessage();
    }   
    //user information correct
    if($_SESSION['USERID']!= ""){ 
         $_SESSION['PERMISSION'] = $perms;
        /* Redirect browser */
    header("Location: ".$_GET['return']);
    } else {
    //user information failed
        header("Location: /#/login?msg=Invalid_Username_or_Password");
    }
}
// Method for logoff
else if( strcasecmp($_GET['method'],'logout') == 0){
    $_SESSION['USERID'] = "";
    $_SESSION['PERMISSION'] = "";
    $session_close();
    header("Location: /#/home");
}
// Method posting content
else if(strcasecmp($_GET['method'],'contentPost')==0){
    $contentQuery = "";
    //if an id parameter exists, update that Id
    if($_GET['id']){
        $contentQuery = "UPDATE content SET content = '".$_POST['content']."', title = '".$_POST['title']."', modified_by = '".$_SESSION['USERID']."', show='".$_POST['show']."', sortorder='".$_POST['order']."', modified_date = CURRENT_TIMESTAMP WHERE id = ".$_GET['id'];
        
    } else{
        //$contentQuery = "INSERT INTO content("title","content","show","sortorder","created_by") VALUES ".
        //    '"'.$_POST['title'].'"'.
        //    '"'.$_POST['content'].'"'.
        //    '"'.$_POST['show'].'"'.
        //    '"'.$_POST['order'].'"'.
        //    '"'.$_SESSION['USERID'].'"';
    }
            //open the database
        $db = new PDO('sqlite:contentDb_PDO.sqlite');
 
        $result = $db->exec(cleanDBtext($contentQuery));
                // close the database connection
        $db = NULL;

} else if(strcasecmp($_GET['method'],'content')==0){
    try
    {
        //open the database
        $db = new PDO('sqlite:contentDb_PDO.sqlite');
        
        $result = $db->query('SELECT id,sortorder,title,show,content FROM content ORDER BY sortorder');
        $myData = '[';
        foreach($result as $row)
        {
            $myData .= '{
       "id": "'.$row['id'].'",
       "order": "'.$row['sortorder'].'",
       "title": "'.$row['title'].'", 
       "show": "'.$row['show'].'", 
       "content": "'.htmlentities($row['content']).'"},';
        }
        $myData = rtrim($myData,",");
        $myData .=']';
        $response['code'] = 1;
        $response['status'] = $api_response_code[ $response['code'] ]['HTTP Response'];
        $response['data'] = $myData;
        deliver_response($_GET['format'], $response);

        // close the database connection
        $db = NULL;
    }
    catch(PDOException $e)
    {
        print 'Exception : '.$e->getMessage();
    }   
    


    // --- Step 4: Deliver Response

    // Return Response to browser
    deliver_response($_GET['format'], $response);
}

?>